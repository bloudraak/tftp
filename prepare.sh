 #!/bin/bash    

INSTALLIP="172.16.13.13";
INSTALLPATH="/volume1/installers/";

#------------------------------------------------------------------------------------------------------------------------------
# Ubuntu Server 14.04.1 64-bit
#------------------------------------------------------------------------------------------------------------------------------
TFTPBOOTDIR="/var/lib/tftpboot/"
MENUPATH="$TFTPBOOTDIR/pxelinux.cfg/default"
mkdir -p "$TFTPBOOTDIR/pxelinux.cfg"
apt-get -y install syslinux
cp /usr/lib/syslinux/pxelinux.0 /var/lib/tftpboot
# cp /usr/lib/syslinux/vesamenu.c32 "$TFTPBOOTDIR"

echo "DEFAULT vesamenu.c32 
TIMEOUT 600
ONTIMEOUT BootLocal
PROMPT 0
MENU INCLUDE pxelinux.cfg/pxe.conf
NOESCAPE 1
LABEL BootLocal
        localboot 0
        TEXT HELP
        Boot to local hard disk
        ENDTEXT
MENU BEGIN Ubuntu
MENU TITLE Ubuntu 
        LABEL Previous
        MENU LABEL Previous Menu
        TEXT HELP
        Return to previous menu
        ENDTEXT
        MENU EXIT
        MENU SEPARATOR
        MENU INCLUDE ubuntu/$OS.menu
MENU ENDa

" > "$MENUPATH";

#------------------------------------------------------------------------------------------------------------------------------
# Ubuntu Server 14.04.1 64-bit
#------------------------------------------------------------------------------------------------------------------------------
OS="ubuntu";
VERSION="14.04.1";
ARCH="amd64";
ARCH_TEXT="64-bit";
PLATFORM="server";
URI="http://releases.ubuntu.com/14.04.1/ubuntu-14.04.1-server-amd64.iso";
TEMP="$OS-$VERSION-$PLATFORM-$ARCH.iso";

OSDIR="$TFTPBOOTDIR/$OS/";
BOOTDIR="$TFTPBOOTDIR/$OS/$VERSION/$PLATFORM/$ARCH";
INSTALLDIR="/srv/install/$OS/$VERSION/$PLATFORM/$ARCH";

#
# Download the ISO
#
if [ ! -f "$TEMP" ]; then
    wget "$URI" -O "$TEMP"
fi

if [ ! -d "$BOOTDIR" ]; then
	mkdir -p "$BOOTDIR"
	mkdir -p "$INSTALLDIR"
	mkdir -p /mnt/loop
	mount -o loop -t iso9660 "$TEMP" /mnt/loop
	cp /mnt/loop/install/vmlinuz "$BOOTDIR"
	cp /mnt/loop/install/initrd.gz "$BOOTDIR"
	cp -R /mnt/loop/* "$INSTALLDIR"
	cp -R /mnt/loop/.disk "$INSTALLDIR"
	umount /mnt/loop
fi

if [ ! -f "$OSDIR/$OS.menu" ]; then
	echo "LABEL 2" > "$OSDIR/$OS.menu";
	echo "\tMENU LABEL Ubuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tKERNEL $OS/$VERSION/$PLATFORM/$ARCH/vmlinuz" >> "$OSDIR/$OS.menu";
	echo "\tAPPEND boot=casper netboot=nfs nfsroot=$INSTALLIP:$INSTALLPATH/$OS/$VERSION/$PLATFORM/$ARCH initrd=$OS/$VERSION/$PLATFORM/$ARCH/initrd.gz" >> "$OSDIR/$OS.menu";
	echo "\tTEXT HELP" >> "$OSDIR/$OS.menu";
	echo "\tUbuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tENDTEXT" >> "$OSDIR/$OS.menu";
fi

#------------------------------------------------------------------------------------------------------------------------------
# Ubuntu Desktop 14.04.1 64bit
#------------------------------------------------------------------------------------------------------------------------------
OS="ubuntu";
VERSION="14.04.1";
ARCH="amd64";
ARCH_TEXT="64-bit";
PLATFORM="desktop";
URI="http://releases.ubuntu.com/14.04.1/ubuntu-14.04.1-desktop-amd64.iso";
TEMP="$OS-$VERSION-$PLATFORM-$ARCH.iso";

OSDIR="/var/lib/tftpboot/$OS/";
BOOTDIR="/var/lib/tftpboot/$OS/$VERSION/$PLATFORM/$ARCH";
INSTALLDIR="/srv/install/$OS/$VERSION/$PLATFORM/$ARCH";

#
# Download the ISO
#
if [ ! -f "$TEMP" ]; then
    wget "$URI" -O "$TEMP"
fi

if [ ! -d "$BOOTDIR" ]; then
	mkdir -p "$BOOTDIR"
	mkdir -p "$INSTALLDIR"
	mkdir -p /mnt/loop
	mount -o loop -t iso9660 "$TEMP" /mnt/loop
	cp /mnt/loop/install/vmlinuz "$BOOTDIR"
	cp /mnt/loop/install/initrd.gz "$BOOTDIR"
	cp -R /mnt/loop/* "$INSTALLDIR"
	cp -R /mnt/loop/.disk "$INSTALLDIR"
	umount /mnt/loop
fi

if [ ! -f "$OSDIR/$OS.menu" ]; then
	echo "LABEL 2" > "$OSDIR/$OS.menu";
	echo "\tMENU LABEL Ubuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tKERNEL $OS/$VERSION/$PLATFORM/$ARCH/vmlinuz" >> "$OSDIR/$OS.menu";
	echo "\tAPPEND boot=casper netboot=nfs nfsroot=$INSTALLIP:$INSTALLPATH/$OS/$VERSION/$PLATFORM/$ARCH initrd=$OS/$VERSION/$PLATFORM/$ARCH/initrd.gz" >> "$OSDIR/$OS.menu";
	echo "\tTEXT HELP" >> "$OSDIR/$OS.menu";
	echo "\tUbuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tENDTEXT" >> "$OSDIR/$OS.menu";
fi

#------------------------------------------------------------------------------------------------------------------------------
# CentOS 7.0
#------------------------------------------------------------------------------------------------------------------------------
OS="centos";
VERSION="7.0";
ARCH="x86_64";
ARCH_TEXT="64-bit";
PLATFORM="desktop";
URI="http://mirror.vtti.vt.edu/centos/7.0.1406/isos/x86_64/CentOS-7.0-1406-x86_64-Minimal.iso";
TEMP="$OS-$VERSION-$PLATFORM-$ARCH.iso";

OSDIR="/var/lib/tftpboot/$OS/";
BOOTDIR="/var/lib/tftpboot/$OS/$VERSION/$PLATFORM/$ARCH";
INSTALLDIR="/srv/install/$OS/$VERSION/$PLATFORM/$ARCH";

#
# Download the ISO
#
if [ ! -f "$TEMP" ]; then
    wget "$URI" -O "$TEMP"
fi

if [ ! -d "$BOOTDIR" ]; then
	mkdir -p "$BOOTDIR"
	mkdir -p "$INSTALLDIR"
	mkdir -p /mnt/loop
	mount -o loop -t iso9660 "$TEMP" /mnt/loop
	cp /mnt/loop/install/vmlinuz "$BOOTDIR"
	cp /mnt/loop/install/initrd.gz "$BOOTDIR"
	cp -R /mnt/loop/* "$INSTALLDIR"
	cp -R /mnt/loop/.disk "$INSTALLDIR"
	umount /mnt/loop
fi

if [ ! -f "$OSDIR/$OS.menu" ]; then
	echo "LABEL 2" > "$OSDIR/$OS.menu";
	echo "\tMENU LABEL CentOS $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tKERNEL $OS/$VERSION/$PLATFORM/$ARCH/vmlinuz" >> "$OSDIR/$OS.menu";
	echo "\tAPPEND boot=casper netboot=nfs nfsroot=$INSTALLIP:$INSTALLPATH/$OS/$VERSION/$PLATFORM/$ARCH initrd=$OS/$VERSION/$PLATFORM/$ARCH/initrd.gz" >> "$OSDIR/$OS.menu";
	echo "\tTEXT HELP" >> "$OSDIR/$OS.menu";
	echo "\tUbuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tENDTEXT" >> "$OSDIR/$OS.menu";
fi

#------------------------------------------------------------------------------------------------------------------------------
# CentOS 6.5
#------------------------------------------------------------------------------------------------------------------------------
OS="centos";
VERSION="6.5";
ARCH="x86_64";
ARCH_TEXT="64-bit";
PLATFORM="desktop";
URI="http://mirror.cs.vt.edu/pub/CentOS/6.5/isos/x86_64/CentOS-6.5-x86_64-minimal.iso";
TEMP="$OS-$VERSION-$PLATFORM-$ARCH.iso";

OSDIR="/var/lib/tftpboot/$OS/";
BOOTDIR="/var/lib/tftpboot/$OS/$VERSION/$PLATFORM/$ARCH";
INSTALLDIR="/srv/install/$OS/$VERSION/$PLATFORM/$ARCH";

#
# Download the ISO
#
if [ ! -f "$TEMP" ]; then
    wget "$URI" -O "$TEMP"
fi

if [ ! -d "$BOOTDIR" ]; then
	mkdir -p "$BOOTDIR"
	mkdir -p "$INSTALLDIR"
	mkdir -p /mnt/loop
	mount -o loop -t iso9660 "$TEMP" /mnt/loop
	cp /mnt/loop/install/vmlinuz "$BOOTDIR"
	cp /mnt/loop/install/initrd.gz "$BOOTDIR"
	cp -R /mnt/loop/* "$INSTALLDIR"
	cp -R /mnt/loop/.disk "$INSTALLDIR"
	umount /mnt/loop
fi

if [ ! -f "$OSDIR/$OS.menu" ]; then
	echo "LABEL 2" > "$OSDIR/$OS.menu";
	echo "\tMENU LABEL CentOS $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tKERNEL $OS/$VERSION/$PLATFORM/$ARCH/vmlinuz" >> "$OSDIR/$OS.menu";
	echo "\tAPPEND boot=casper netboot=nfs nfsroot=$INSTALLIP:$INSTALLPATH/$OS/$VERSION/$PLATFORM/$ARCH initrd=$OS/$VERSION/$PLATFORM/$ARCH/initrd.gz" >> "$OSDIR/$OS.menu";
	echo "\tTEXT HELP" >> "$OSDIR/$OS.menu";
	echo "\tUbuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tENDTEXT" >> "$OSDIR/$OS.menu";
fi


#------------------------------------------------------------------------------------------------------------------------------
# CentOS 6.5
#------------------------------------------------------------------------------------------------------------------------------
OS="centos";
VERSION="6.5";
ARCH="i386";
ARCH_TEXT="32-bit";
PLATFORM="desktop";
URI="http://mirror.vtti.vt.edu/centos/6.5/isos/i386/CentOS-6.5-i386-minimal.iso";
TEMP="$OS-$VERSION-$PLATFORM-$ARCH.iso";

OSDIR="/var/lib/tftpboot/$OS/";
BOOTDIR="/var/lib/tftpboot/$OS/$VERSION/$PLATFORM/$ARCH";
INSTALLDIR="/srv/install/$OS/$VERSION/$PLATFORM/$ARCH";

#
# Download the ISO
#
if [ ! -f "$TEMP" ]; then
    wget "$URI" -O "$TEMP"
fi

if [ ! -d "$BOOTDIR" ]; then
	mkdir -p "$BOOTDIR"
	mkdir -p "$INSTALLDIR"
	mkdir -p /mnt/loop
	mount -o loop -t iso9660 "$TEMP" /mnt/loop
	cp /mnt/loop/install/vmlinuz "$BOOTDIR"
	cp /mnt/loop/install/initrd.gz "$BOOTDIR"
	cp -R /mnt/loop/* "$INSTALLDIR"
	cp -R /mnt/loop/.disk "$INSTALLDIR"
	umount /mnt/loop
fi

if [ ! -f "$OSDIR/$OS.menu" ]; then
	echo "LABEL 2" > "$OSDIR/$OS.menu";
	echo "\tMENU LABEL CentOS $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tKERNEL $OS/$VERSION/$PLATFORM/$ARCH/vmlinuz" >> "$OSDIR/$OS.menu";
	echo "\tAPPEND boot=casper netboot=nfs nfsroot=$INSTALLIP:$INSTALLPATH/$OS/$VERSION/$PLATFORM/$ARCH initrd=$OS/$VERSION/$PLATFORM/$ARCH/initrd.gz" >> "$OSDIR/$OS.menu";
	echo "\tTEXT HELP" >> "$OSDIR/$OS.menu";
	echo "\tUbuntu Server $VERSION ($ARCH_TEXT)" >> "$OSDIR/$OS.menu";
	echo "\tENDTEXT" >> "$OSDIR/$OS.menu";
fi

echo "";
echo "Done...";